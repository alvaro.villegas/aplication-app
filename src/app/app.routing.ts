import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import {HomeComponent}from './feature/home/home.component';
import { AboutUsComponent } from './feature/about-us/about-us.component';
import { BlogComponent } from './feature/blog/blog.component';
import {SettingsComponent} from './feature/settings/settings.component';

const appRoutes = [
  {path: '',pathMatch: 'full',redirectTo: 'home' },
  {path: 'home', component: HomeComponent,  pathMatch: 'full'},
  {path: 'about-us', component: AboutUsComponent},
  {path: 'blog', component: BlogComponent},
  {path: 'settings', component: SettingsComponent},
];
export const routing = RouterModule.forRoot(appRoutes);